import random
import scipy
from scipy import linalg
import numpy
import copy
import logging
from IPython import embed
import pdb

LOG_FORMAT = '%(message)s'
LOG_LEVEL = logging.DEBUG

logger = logging.getLogger('')

logger.setLevel(LOG_LEVEL)
ch = logging.StreamHandler()
ch.setLevel(LOG_LEVEL)

formatter = logging.Formatter(LOG_FORMAT)
ch.setFormatter(formatter)

logger.addHandler(ch)


def chooseFromWeightedVector(weights):
    """Randomly choose an ndx from a vector of weighted probabilities.

    A choice from [.2, .8] will return 0 20% of the time and 1 80% of
    the time.
    """

    assert abs(sum(weights) - 1.0) < 1e-5, "Weight vector ({0}) must sum to 1.0".format(weights)

    r = random.random()
    running_total = 0.0
    for (ndx, weight) in enumerate(weights):
        running_total += weight
        if running_total >= r:
            return ndx
    else:
        return ndx

def generateData(vector_choices_pre, vector_choices_post, numerical_pre, numerical_post):
    pre_set = generateDistribution(40, vector_choices_pre, numerical_pre)
    post_set = generateDistribution(40, vector_choices_post, numerical_post)

    return pre_set, post_set

def generateDistribution(number_rows, vector_choices, numerical_stats):
    num_mean, num_std = numerical_stats

    def _generateRow():
        vals = map(chooseFromWeightedVector, vector_choices)
        return vals

    rows = [_generateRow() for x in xrange(number_rows)]
    B = numpy.random.normal(num_mean, num_std, number_rows)

    mapped_rows = map(lambda x: mapRowsToIndicators(vector_choices, x), rows)
    return numpy.vstack(mapped_rows), B

def mapRowsToIndicators(vector_choices, row):
    zeroes = map(lambda x: numpy.zeros(len(x)-1), vector_choices)
    for (ndx, x) in enumerate(row):
        try:
            zeroes[ndx][x] = 1
        except IndexError:
            # We count this as a Nan
            pass
    return numpy.hstack(zeroes)

def _makeVarRangeArray(var_mask):
    var_indexes = []
    for x in var_mask:
        if not var_indexes:
            var_indexes.append((0, len(x)))
        else:
            var_indexes.append((var_indexes[-1][-1], var_indexes[-1][-1] + len(x)))
    return var_indexes

def filterMatrices(combined_A, var_mask):

    var_mask_vector = numpy.hstack(var_mask)

    var_usage_mask = copy.deepcopy(var_mask) # Evidentally some variables are used for building but not predicting in the model.
    row_mask = numpy.ones(combined_A.shape[0])

    # This takes the var mask list and turns it into a list of ranges.
    var_indexes = _makeVarRangeArray(var_mask)

    for (var_ndx, (start_ndx, end_ndx)) in enumerate(var_indexes):
        var_cols = combined_A[:,start_ndx:end_ndx]

        # If more than 10% of the buildings in the peer group are missing data for a numerical variable,
        # remove the variable from the model.
        row_has_indicator = numpy.any(var_cols, 1)
        if numpy.count_nonzero(row_has_indicator) / float(row_has_indicator.size) < .9:
            logger.debug('Removing variable {0} because >10% of buildings are missing data ({1}%).'.format(var_ndx,
                                                                                                           numpy.count_nonzero(row_has_indicator) / float(row_has_indicator.size) * 100.0))
            for ndx in xrange(len(var_mask[var_ndx])): var_mask[var_ndx][ndx] = False
                
        #  Otherwise, remove the buildings that are missing the
        # data from the peer group. These buildings should not be used to compute model coefficients,
        # nor to compute savings predictions.                
        for row_ndx in xrange(row_mask.size):
            if not numpy.any(var_cols[row_ndx]) and numpy.any(var_mask[var_ndx]):
                logger.debug("Removing row {0} because it does not not have data for variable {1}".format(row_ndx, var_ndx))
                row_mask[row_ndx] = False

        # If more than 95% of the buildings in the peer group have the same value for a categorical
        # 5 of 6variable, remove the variable from the model. Savings predictions cannot be made for any
        # values of the removed variable.

        # If between zero and 5% of the buildings in the peer group have the same value for a categorical
        # variable, the individual value of the variable should remain in the model, but cannot be used
        # to make savings predictions. Other values of the variable can still be used to make savings
        # predictions.
        col_sum = numpy.sum(numpy.compress(row_mask, var_cols, axis=0), axis=0)
        thresh_1 = var_cols.shape[0] * .95
        thresh_2 = var_cols.shape[0] * 0.05

        if numpy.any(col_sum > thresh_1):
            logger.debug("Removing variable {0} b/c > {1} have a single value")
            for ndx in xrange(len(var_mask[var_ndx])): var_mask[var_ndx][ndx] = False

        #  If none of buildings in the peer group have a value of a categorical variable, that value of the
        # variable should be removed from the model. The remaining values of the variable may remain
        # in the model
        for (ndx, val) in enumerate(col_sum < thresh_2):
            if not var_usage_mask[var_ndx][ndx]: continue # Not strictly necessary.
            if val:
                logger.debug("Setting enum {0} of var {1} to not be usable b/c fewer than 5% have this value.".format(ndx, var_ndx))                
                var_usage_mask[var_ndx][ndx] = False


    # NOTE NOTE NOTE
    # I don't understand step e) in the filtration process.
                
    return var_mask, var_usage_mask, row_mask

def main(pre_A, pre_B, post_A, post_B, var_mask, pre_retro_vect, post_retro_vect):
    """Simple toy model for Travis' regression model.  Takes matrices
    representing a data for a set of buildings (in indicator variable
    format), a var_mask that is a list of lists showing how the matrix
    breaks down into indicator variables, and pre/post retro vects
    which show before/after retrofits within the peer groups.
    """

    print("Building model on pre_A:")
    print(pre_A)
    print("pre_B:")
    print(pre_B)
    print("post_A:")
    print(post_A)
    print("post_B:")
    print(post_B)
    print("pre_retro_vect: {0}".format(pre_retro_vect))
    print("post_retro_vect: {0}".format(post_retro_vect))
          

    A_unfiltered = numpy.vstack((pre_A, post_A))
    B_unfiltered = numpy.hstack((pre_B, post_B))

    # This produces a list of masks, which are vectors/lists of lists
    # that show whether a row/column should be included.
    (var_list_mask, var_usage_mask, row_mask) = filterMatrices(A_unfiltered, var_mask)

    # Compress the matrix in using the var and row masks.
    var_mask = numpy.hstack(var_list_mask)

    A = numpy.compress(var_mask, A_unfiltered, axis=1)
    A = numpy.compress(row_mask, A, axis=0)

    # Add a column of ones to the col0 spot.  
    A = numpy.hstack( (numpy.ones((A.shape[0], 1)), A))

    pre_A = numpy.compress(var_mask, pre_A, axis=1)

    # This is a bit ridiculous but the first portion of the row mask
    # corresponds to the pre_A matrix.
    pre_A = numpy.compress(row_mask[:pre_A.shape[0]], pre_A, axis=0)
    pre_A = numpy.hstack( (numpy.ones((pre_A.shape[0], 1)), pre_A))

    pre_retro_vect  = numpy.hstack( ([1], numpy.compress(var_mask, pre_retro_vect)))
    post_retro_vect = numpy.hstack( ([1], numpy.compress(var_mask, post_retro_vect)))

    B = numpy.compress(row_mask, B_unfiltered, axis=0)

    num_rows, num_cols = A.shape
    assert num_rows >= num_cols, "Cannot build model unless #rows >= #cols( num_rows={0}, num_cols={1}".format(*A.shape)
    assert num_rows >= 30, "Cannot build model with fewer than 30 buildings."

    print("Solving {0}x{1} system".format(num_rows, num_cols))
    (X, residues, rank, s) = linalg.lstsq(A, B)

    print("X: {0}".format(X))
    
    pre_building_set = numpy.compress(numpy.dot(pre_A, pre_retro_vect) - 1, pre_A, axis=0)

    # Applying a bunch of masks.  But it basically amounts to "make
    # the matrix where the value of every element in a row is the
    # value of the same element in the original if post_retro_vect is
    # 0 there and 0 if post_retro_vect is 1"
    post_building_set = numpy.logical_or(numpy.logical_and(pre_building_set,
                                                           numpy.logical_not(pre_retro_vect)),
                                         post_retro_vect).astype(float)

    expected_pre = numpy.dot(pre_building_set, X)
    expected_post = numpy.dot(post_building_set, X)
    expected_diff = expected_post - expected_pre

    # This "works" but can't be correct, because every value in
    # the matrix ends up being A(post_retro_vect) - A(pre_retro_vect).

    # It's a meaningfull number, it just isn't that interesting.
    expected_val = numpy.mean(expected_diff)

    print("Expected difference is {0}".format(expected_val))
    return 


if __name__ == '__main__':

    # [.1, .1, .8] means there is a .1 chance of a 0, .1 chance of 1,
    # and a .8 chance of NaN.
    choices_pre = [[0.45, 0.01, 0.24, 0.3], [.2, .2, .55, .05], [.1, .7, .13, .07]]
    choices_post = [[.4, .06, .49, .05], [.4, .2, .35, .05], [.1, .17, .7, .03]]

    # These define means and std for gaussians for the pre/post
    # observation variables.
    meanstd_pre = (100., 10.)
    meanstd_post = (150.0, 30.)

    assert len(choices_pre) == len(choices_post), ("Choice vectors must have same length."
                                                   "len({0}) != len({1}).".format(len(choices_pre),
                                                                                  len(choices_post)))

    for (ndx, (pre_arr, post_arr)) in enumerate(zip(choices_pre, choices_post)):
        assert len(pre_arr) == len(post_arr), ("Choice elements must have same length but "
                                               "do not at ndx {0}. "
                                               "len({1}) != len({2})".format(ndx,
                                                                             len(pre_arr),
                                                                             len(post_arr)))

    var_mask = [[True for emnt in in_array[:-1]] for in_array in choices_pre]

    # Generate peer groups randomly.
    (pre_A, pre_B) = generateDistribution(40, choices_pre, meanstd_pre)
    (post_A, post_B) = generateDistribution(40, choices_post, meanstd_post)

    # This basically represents a change from the 2nd value of the 2nd
    # variable to the 3rd value of the 2nd variable.
    pre_retro_vect  = [0,0,0,  0,1,0,  0,0,0]
    post_retro_vect = [0,0,0,  0,0,1,  0,0,0]
    main(pre_A, pre_B, post_A, post_B, var_mask, pre_retro_vect, post_retro_vect)
